package fr.iutlille.ctp_2122;

/* TODO Q3: RecyclerView */
    /* TODO Q3a: Menu contextuel */
    /* TODO Q5: EditItem */

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import fr.iutlille.ctp_2122.databinding.ActivityCandidatureBinding;
import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class CandidatureViewHolder extends RecyclerView.ViewHolder {

    private final ActivityCandidatureBinding ui;
    private CandidatureAdapter.OnItemClickListener listener;

    public CandidatureViewHolder(ActivityCandidatureBinding ui) {
        super(ui.getRoot());
        this.ui = ui;
        itemView.setOnClickListener((View.OnClickListener) this);
    }

    public void setCandidature(Candidature candidature) {
        ui.etCandidature
    }

    public void setOnItemClickListener(CandidatureAdapter.OnItemClickListener l)  {
        this.listener = l;
    }

    @Override
    public void onClick(View v) {
        if (listener != null)
            listener.onItemClick(getAbsoluteAdapterPosition());
    }
}