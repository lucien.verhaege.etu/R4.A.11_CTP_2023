package fr.iutlille.ctp_2122;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.iutlille.ctp_2122.databinding.ActivityCandidatureBinding;
import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class CandidatureAdapter extends RecyclerView.Adapter<CandidatureViewHolder> {

    private List<Candidature> candidatureList;

    CandidatureAdapter(List<Candidature> candidatures) {
        this.candidatureList = candidatures;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener l) {
        this.listener = l;
    }

    @NonNull
    @Override
    public CandidatureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityMainBinding binding = ActivityMainBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false);
        return new CandidatureViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidatureViewHolder holder, int position) {
        Candidature xmen = candidatureList.get(position);
        holder.setCandidature(xmen);
        holder.setOnItemClickListener(this.listener);
    }

    @Override
    public int getItemCount() {
        return this.candidatureList.size();
    }
}