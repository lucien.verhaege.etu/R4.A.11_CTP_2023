package fr.iutlille.ctp_2122;  // ligne à modifier


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private final int active = 0;
    private ActivityMainBinding ui;
    private CandidatureAdapter adapter;
    private List<Candidature> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(ui.getRoot());
        ParcoursBUT application = (ParcoursBUT) getApplication();
        /* Q1: statistiques */
        TextView probabilites = findViewById(R.id.Title);
        probabilites.setText(application.getCandidatures().size() + " prospects");

        int nbNope = 0;
        int nbDone = 0;
        int nbLate = 0;
        int nbTodo = 0;
        for(Candidature c : application.getCandidatures()){
            if(c.getEtat().equals(Candidature.Etat.DONE)){
                nbDone++;
            } else if(c.getEtat().equals(Candidature.Etat.NOPE)){
                nbNope++;
            } else if(c.getEtat().equals(Candidature.Etat.LATE)){
                nbLate++;
            } else {
                nbTodo++;
            }
        }
        TextView nope = findViewById(R.id.nbNope);
        nope.setText(nbNope + "");
        TextView done = findViewById(R.id.nbDone);
        done.setText(nbDone + "");
        TextView late = findViewById(R.id.nbLate);
        late.setText(nbLate + "");
        TextView todo = findViewById(R.id.nbTodo);
        todo.setText(nbTodo + "");

        /* Q2 : A propos */
        Intent intent = new Intent(this, AboutActivity.class);
        Button btnApropos = findViewById(R.id.btnApropos);
        btnApropos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });

        adapter = new CandidatureAdapter(application.getCandidatures());
        adapter.setOnItemClickListener((CandidatureAdapter.OnItemClickListener) this);
        list = application.getCandidatures();

        // adapteur fourni au recyclerView
        ui.recyclerView.setAdapter(adapter);
        ui.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        ui.recyclerView.setLayoutManager(lm);
        /* TODO Q3a: Menu contextuel */
        /* TODO Q3: RecyclerView */
    }

}