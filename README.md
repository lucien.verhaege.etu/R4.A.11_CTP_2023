# R4.A.11_Contrôle_TP_22-23

## Description du projet

Le projet `Parcours_BUT` doit permettre à un étudiant de BUT de gérer ses candidatures à d'éventuelles poursuites d'études.

Une candidature (quelquefois appelé dossier) est composée d'un nom (le nom de la formation envisagée) et d'une date limite de dépôt de dossier.
De plus on pourra annoter ce dossier selon un état parmi les 4 suivants :

* NOPE (Ignoré) : La formation figure bien dans la liste, mais l'étudiant n'est pas intéressé.
* TODO (Prévu) : L'étudiant souhaite postuler, mais n'a pas encore faite le dossier.
* DONE (Fait) : L'étudiant est intéressé et a expédié le dossier.
* LATE (En retard) : L'étudiant est intéressé, mais n'a pas encore envoyé son dossier alors que la date limite est passée.

Chacun de ces 4 états est représenté par une icône (existant déjà en tant que ressource dans l'application).

## Déroulement du contrôle TP

### Mise en place

Vous venez de cloner ce dépôt qui contient :

* Ce sujet de contrôle TP
* L'ensemble des sources et ressources dont vous aurez besoin pour ce contrôle.

### Aide et assistance

* Vous n'arriverez peut-être pas au bout de ce contrôle très volumineux. Pas de panique ! Le barème sera sur plus de 20 points.

* Un jeu d'essai est préchargé à partir du fichier json (présent comme une ressource 'raw' de nom `cand.json`).

* Le panel 'TODO' vous indiquera l'ensemble des endroits où vous devrez intervenir pour réaliser chacune des questions (il peut y en avoir plusieurs par question).

* Comme indiqué sur le graphique ci-dessous:
  * les questions  1, 2, 3 et 4 sont indépendantes.
  * les questions 3a et 3b sont indépendantes entre elles, mais suivent forcément la question 3
  * la question 5 ne peut être faite qu'après la 3 et la 4 (3a et 3b non nécessaires).

![Structure](./figures/structure.png)

* la commande

```bash
adb install ./demo.apk
```

exécutée depuis le volet 'Terminal' installe une version complète du programme ('`Parcours_BUT (DEMO)`')sur votre android virtuel. 

### Restitution

* Ajoutez votre enseignant comme développeur sur votre dépôt gitlab (que vous venez juste de fork)

* Apres chaque question (votre projet doit compiler et être fonctionnel), vous enregistrerez l'état de votre projet. 
    Rappel des commandes:

```bash
git commit -am 'Question X'
git tag Qx
git push --tag # peut être éventuellement différé en fin de tp
```  

#### Rappels git

* La commande:

```bash
git config --global credential.helper cache
```

vous évitera d'avoir à ressaisir vos identifiants/mot de passe à chaque fois.

* Les commandes suivantes vous permettront de compléter une question déjà validée (par exemple la Qx):

```bash
git tag -d Qx
git commit -am 'Question X (complément)'
git tag Qx
git push --tag # peut être éventuellement différé en fin de tp
```


## Question 1. Statistiques

A l'aide de la classe ParcoursBUT, complétez la classe MainActivity pour que:

* le nombre de dossiers total apparaisse à la place du titre 'STATISTIQUES' sous la forme: '9 prospects'
* le nombre de dossiers dans chaque état soit affiché sur cette activité (en dessous de l'icône correspondante)

_Note_: 

* l'ensemble des calculs/mises à jour sera encapsulée dans une méthode `MainActivity.showStats()` (à écrire) appelée à l'initialisation de l'activité.
* la méthode `ParcoursBUT.getCandidatures()` (fournie) permet d'obtenir le liste des candidatures.
* la méthode `Candidature.getEtat()` (fournie) permet de connaître l'état d'une candidature.
* Vous pouvez utiliser la ressource chaine `nbDossiers` pour calculer la chaine de titre. Alternativement vous pouvez aussi utiliser la ressource `R.plurals.nbCandidatures`.
  * ([Documentation sur les ressources formattées](https://developer.android.com/guide/topics/resources/string-resource?hl=fr#formatting-strings)) 
  * ([documentation sur les chaînes 'quantités'](https://developer.android.com/guide/topics/resources/string-resource?hl=fr#Plurals))

Le résultat devrait être:

![Question 1](figures/Q1.png)

```bash
git commit -am 'Q1'
git tag Q1
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 2. A propos

Complétez le projet pour qu'un clic sur le bouton 'A propos' en haut et à droite de l'activité affiche l'activité 'AboutActivity' (fournie).
Le retour sur l'écran principal se fera classiquement avec la touche `BACK` de l'équippement.

```bash
git commit -am 'Q2'
git tag Q2
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 3. RecyclerView

Complétez le projet pour que le composant RecyclerView de l'activité principale affiche le nom, la date et l'icône (selon l'état du dossier)
associé à une candidature.

Le résultat devrait être:

![Question 3](figures/Q3.png)

_Note_:

* Si possible, affichez la date selon le format fixé par le système (régler en français pour voir).
* Les fichiers vides `CandidatureAdapter.java` et `CandidatureViewHolder.java` sont déjà présents dans le git pour accueillir les classes à développer.

```bash
git commit -am 'Q3'
git tag Q3
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 3a. Menu contextuel

Complétez le projet pour ajouter un menu contextuel actif sur les éléments de la liste et permettant de changer l'état de l'item concerné (il y aura donc 4 éléments dans ce menu)

_Notes_:

* Utilisez la méthode `ParcoursBUT.setStatus(int position, Candidature.Etat newStatus)` (fournie)
* Pensez à rappeler la méthode `showStats` de la question 1 pour mettre à jour les statistiques.

Le résultat devrait être:

![Question 3a](figures/Q3a.png)

```bash
git commit -am 'Q3a'
git tag Q3a
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 3b. Tri de la liste

Complétez le projet pour que les boutons 'Tri par date ' et 'Tri par état' soient fonctionnels (ils permettent un affichage trié du RecyclerView).

_Notes_:

* Utilisez les méthodes `ParcoursBUT.sortByDate()` et `ParcoursBUT.sortByStatus()` (fournies).
* N'oubliez pas de réactualiser la liste.

```bash
git commit -am 'Q3b'
git tag Q3b
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 4. Création d'un layout

Concevez le layout  'activity_candidature.xml' pour être le plus proche possible de l'affichage suivant:

![Question 3a](figures/Q4.png)

_Notes_:

* L'élément de saisie de text doit avoir pour id : `edtNom`
* L'élément calendrier doit avoir pour id : `edtLimite`
* Le RadioGroup doit avoir pour id : `edtEtat` et les id des boutons inclus respectivement `rbNope`,`rbTodo`,`rbDone`,`rbLate`. 
(le code fourni `CandidatureActivity` utilise ces identifiants.)
* Vous ne pourrez pas visualiser cet écran dans votre application avant la question 5.

```bash
git commit -am 'Q4'
git tag Q4
git push --tag # peut être éventuellement différé en fin de tp
```  

## Question 5. EditItem

Complétez le projet pour que le bouton doté du crayon embraye vers l'activité 'CandidatureActivity'. La position de l'élément sujet de l'action est passée comme données 'extra' de l'intent (entier de nom "position")

_Alternatives_:

* Si vous avez fait la Question 3a, vous pouvez simplement ajouter un élément 'Edition' au menu contextuel (au lieu d'utiliser le bouton situé dans l'item),
* vous pouvez également détecter un simple clic n'importe où sur l'item.

_Note_:

* Dé-commentez le reste de la classe `CandidatureActivity`.
* Ajoutez un écouteur sur le click du bouton visé.
* En plus de `CandidatureViewHolder`, vous aurez à modifier aussi `MainActivity` et `CandidatureAdapter`.
* Le bouton 'Valider' de l'activité d'édition de candidature doit appeler la méthode `CandidatureActivity.doValider()`.

```bash
git commit -am 'Q5'
git tag Q5
git push --tag # peut être éventuellement différé en fin de tp
```  
